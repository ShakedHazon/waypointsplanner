export default class RouteClass {
    constructor({start = undefined, locations = [], distance = undefined, end = undefined} = {}) {
        this.start = start;
        this.locations = locations;
        this.distance = distance;
        this.end = end;
    }
}