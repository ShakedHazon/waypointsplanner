export default class LocationClass {
    constructor({longitude, latitude, address = undefined}) {
        this.longitude = longitude;
        this.latitude = latitude;
        this.address = address;
    }

    get geoString() {
        return `geo!${this.latitude},${this.longitude}`;
    }

    get label() {
        return LocationClass.getLabel(this.address);
    }

    static getLabel(address = {}) {
        let {Street = '', HouseNumber = '', City = ''} = address;
        const label = Street ? `${Street} ${HouseNumber}, ${City}` : `${City}`;
        return address && label ? label : undefined;
    }

}