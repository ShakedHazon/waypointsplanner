import React from 'react';
import {stringToLocation, getSuggestions, debounceFunc} from '../BL/BL';
import TextField from "@material-ui/core/es/TextField/TextField";
import PropTypes from 'prop-types';
import Autosuggest from 'react-autosuggest';
import match from 'autosuggest-highlight/match';
import parse from 'autosuggest-highlight/parse';
import MenuItem from "@material-ui/core/es/MenuItem/MenuItem";
import Paper from "@material-ui/core/es/Paper/Paper";
import HomeIcon from '@material-ui/icons/HomeOutlined';
import PlaceIcon from '@material-ui/icons/PlaceOutlined';
import FlagIcon from '@material-ui/icons/FlagOutlined';
import Grid from "@material-ui/core/es/Grid/Grid";

export default class LocationInput extends React.Component {

    state = {value: '', suggestions: []};

    static propTypes = {
        label: PropTypes.string,
        required: PropTypes.bool,
        type: PropTypes.number,
        onSubmit: PropTypes.func.isRequired
    };

    static defaultProps = {
        label: 'Destination',
        required: false,
        type: 0
    };

    enterLocation = (address) => {
        this.setState(() => ({value: ''}));
        stringToLocation(address.trim()).then((loc) => {
            this.props.onSubmit(loc);
        });
    };

    onSubmit = (e) => {
        e.preventDefault();
        const address = e.target.elements[0].value;
        this.enterLocation(address);
    };

    onSuggestionsFetchRequested = debounceFunc(({value}) => {
        getSuggestions(value.trim().toLowerCase()).then((suggestions) => {
            this.setState(() => ({suggestions}))
        });
    }, 300);

    getSuggestionValue = (suggestion) => suggestion;

    renderSuggestion = (suggestion, {query, isHighlighted}) => {
        const matches = match(suggestion, query);
        const parts = parse(suggestion, matches);

        return (
            <MenuItem component="div">
                <div>
                    {parts.map((part, index) => {
                        return part.highlight ? (
                            <span key={String(index)} style={{fontWeight: 700}}>
              {part.text}
            </span>
                        ) : (
                            <strong key={String(index)} style={{fontWeight: 300}}>
                                {part.text}
                            </strong>
                        );
                    })}
                </div>
            </MenuItem>
        );
    };

    onSuggestionsClearRequested = () => {
        this.setState(() => ({suggestions: []}));
    };

    onSuggestionSelected = (event, {suggestion, suggestionValue, suggestionIndex, sectionIndex, method}) => {
        this.enterLocation(suggestion);
    };

    renderInputComponent = (inputProps) => {
        const {value, onChange, label, required} = inputProps;
        const icons = [<PlaceIcon/>, <HomeIcon/>, <FlagIcon/>];
        return <Grid container spacing={8} alignItems="flex-end">
            <Grid item>
                {icons[this.props.type]}
            </Grid>
            <Grid item>
                <TextField
                    inputProps={inputProps}
                    value={value}
                    onChange={onChange}
                    label={label}
                    required={required}
                    margin="normal"
                />
            </Grid>
        </Grid>;
    };

    onChange = (event, {newValue}) => {
        this.setState({
            value: newValue
        });
    };

    render() {
        return <div>
            <form onSubmit={this.onSubmit}>
                <Autosuggest
                    suggestions={this.state.suggestions}
                    onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                    onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                    getSuggestionValue={this.getSuggestionValue}
                    renderSuggestion={this.renderSuggestion}
                    onSuggestionSelected={this.onSuggestionSelected}
                    renderInputComponent={this.renderInputComponent}
                    renderSuggestionsContainer={options => (
                        <Paper {...options.containerProps} square>
                            {options.children}
                        </Paper>)}
                    inputProps={{value: this.state.value, onChange: this.onChange, ...this.props}}
                    theme={{
                        suggestionsList: {listStyleType: 'none', margin: 0, padding: 0},
                        suggestion: {display: 'block'},
                        suggestionsContainerOpen: {position: 'absolute', zIndex: 3}
                    }}
                />
            </form>
        </div>
    }
}
