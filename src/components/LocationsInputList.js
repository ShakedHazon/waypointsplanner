import React from 'react';
import LocationInput from "./LocationInput";
import Button from "@material-ui/core/es/Button/Button";
import NavigationIcon from '@material-ui/icons/Navigation'
import Route from "./Route";
import RouteClass from "../classes/Route.class";
import LocationClass from "../classes/Location.class";
import Checkbox from "@material-ui/core/es/Checkbox/Checkbox";
import FormControlLabel from "@material-ui/core/es/FormControlLabel/FormControlLabel";
import FormGroup from "@material-ui/core/es/FormGroup/FormGroup";

const [SAVE_DATA, START, END] = ['saveData', 'start', 'end'];
export default class LocationsInputList extends React.Component {
    state = {route: new RouteClass(), saveData: true};

    updateRoute = (prevState, newProps) => (
        {route: new RouteClass({...prevState.route, ...newProps})}
    );

    componentDidMount = () => {
        const startObj = JSON.parse(localStorage.getItem(START));
        const endObj = JSON.parse(localStorage.getItem(END));
        const saveData = JSON.parse(localStorage.getItem(SAVE_DATA));
        const start = startObj ? new LocationClass(startObj) : undefined;
        const end = endObj ? new LocationClass(endObj) : undefined;
        if (saveData === null || saveData === true) {
            this.setState((prevState) => ({
                saveData: true,
                ...this.updateRoute(prevState, {start, end})
            }));
        }
        else if (saveData === false)
            this.setState(() => ({saveData: false}));
    };

    // region Callbacks
    onAdd = (newLocation) => {
        this.setState((prevState) => (this.updateRoute(prevState, {locations: [...prevState.route.locations, newLocation]})));
    };

    onAddStart = (start) => {
        localStorage.setItem(START, JSON.stringify(start));
        this.setState((prevState) => (this.updateRoute(prevState, {start})))
    };

    onAddEnd = (end) => {
        localStorage.setItem(END, JSON.stringify(end));
        this.setState((prevState) => (this.updateRoute(prevState, {end})))
    };

    onDelete = (location) => {
        this.setState((prevState) => (this.updateRoute(prevState, {locations: prevState.route.locations.filter((l) => l.geoString !== location.geoString)})));
    };

    onDeleteStart = () => {
        localStorage.removeItem(START);
        this.setState((prevState) => (this.updateRoute(prevState, {start: undefined})))
    };

    onDeleteEnd = () => {
        localStorage.removeItem('end');
        this.setState((prevState) => (this.updateRoute(prevState, {end: undefined})))
    };

    onBuild = () => {
        this.props.buildRoute(this.state.route);
    };

    onChangeSaveData = () => {
        if (!this.state.saveData === false)
            localStorage.clear();
        localStorage.setItem(SAVE_DATA, !this.state.saveData);
        this.setState((prevState) => ({saveData: !prevState.saveData}));
    };

    //endregion

    render() {
        return <React.Fragment>
            <Route
                route={this.state.route}
                handleDeleteStart={this.onDeleteStart}
                handleDelete={this.onDelete}
                handleDeleteEnd={this.onDeleteEnd}/>

            {!this.state.route.start && <LocationInput
                required
                label={'Start of route'}
                type={1}
                onSubmit={this.onAddStart}/>}
            <LocationInput
                label={'Stop in route'}
                onSubmit={this.onAdd}/>
            {!this.state.route.end && <LocationInput
                label={'End of route'}
                type={2}
                onSubmit={this.onAddEnd}/>}
            <Button
                onClick={this.onBuild}
                variant="extendedFab"
                aria-label="Calculate Route"
                disabled={!this.state.route.start || (this.state.route.locations.length <= 0 && !this.state.route.end)}>
                <NavigationIcon/>
                Calculate Route
            </Button>
            <FormGroup row={false}>
                <FormControlLabel
                    control={
                        <Checkbox
                            checked={this.state.saveData}
                            onChange={this.onChangeSaveData}
                            value="checkedA"
                        />
                    }
                    label="Save start & end point for next uses"
                />
            </FormGroup>
        </React.Fragment>
    }
}