import React from "react";
import DistanceCalculator from './DistanceCalculator';
import Header from "./Header";
import MuiThemeProvider from "@material-ui/core/es/styles/MuiThemeProvider";
import createMuiTheme from "@material-ui/core/es/styles/createMuiTheme";
import Paper from "@material-ui/core/es/Paper/Paper";

const theme = createMuiTheme({
    palette: {
        primary: {
            main: '#1565c0',
        },
        secondary: {
            main: '#0277bd',
        },
    },
});

export default () => (
    <MuiThemeProvider theme={theme}>
        <Header/>
        <Paper style={{margin: '1vh', height: '88vh', textAlign: 'center', overflow: 'auto'}}>
            <DistanceCalculator/>
        </Paper>
    </MuiThemeProvider>)
