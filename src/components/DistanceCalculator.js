import React from "react";
import LocationsInputList from "./LocationsInputList";
import {getBestRoute} from '../BL/BL';
import AutorenewIcon from '@material-ui/icons/Autorenew';
import Button from "@material-ui/core/es/Button/Button";
import Route from "./Route";
import Grid from "@material-ui/core/es/Grid/Grid";

export default class DistanceCalculator extends React.Component {
    state = {route: undefined};

    resetRoute = () => {
        this.setState(() => ({route: undefined}))
    };

    buildRoute = (route) => {
        getBestRoute(route).then((plannedRoute) => {
            this.setState(() => ({route: plannedRoute}));
        })
    };

    render() {
        return <Grid container
                     direction="column"
                     justify="center"
                     alignItems="center">

            <Grid item>
                {!this.state.route && <LocationsInputList buildRoute={this.buildRoute}/>}
                {this.state.route && <Route route={this.state.route}/>}
            </Grid>
            {this.state.route &&
            <Grid item>
                <Button
                    onClick={this.resetRoute}
                    variant="extendedFab"
                    aria-label="Calculate Route">
                    <AutorenewIcon/>
                    New Route
                </Button>
            </Grid>}
        </Grid>
    }
}