import React from 'react';
import PropTypes from "prop-types";
import RouteClass from '../classes/Route.class';
import Location from "./Location";
import Typography from "@material-ui/core/es/Typography/Typography";
import List from "@material-ui/core/es/List/List";
import ListItem from "@material-ui/core/es/ListItem/ListItem";

export default class Route extends React.Component {
    static propTypes = {
        route: PropTypes.instanceOf(RouteClass).isRequired,
        handleDelete: PropTypes.func,
        handleDeleteStart: PropTypes.func,
        handleDeleteEnd: PropTypes.func
    };

    render() {
        return <List>
            {this.props.route.start &&
            <ListItem>
                <Location
                    handleDelete={this.props.handleDeleteStart}
                    type={1}
                    location={this.props.route.start}/>
            </ListItem>}
            {this.props.route.locations && this.props.route.locations.map((l) => (
                <ListItem
                    key={l.geoString}>
                    <Location
                        location={l}
                        handleDelete={this.props.handleDelete}/>
                </ListItem>
            ))}
            {this.props.route.end &&
            <ListItem>
                <Location
                    location={this.props.route.end}
                    type={2}
                    handleDelete={this.props.handleDeleteEnd}
                />
            </ListItem>}
            {this.props.route.distance &&
            <ListItem>
                < Typography>
                    Total Distance: {this.props.route.distance} meters
                </Typography>
            </ListItem>
            }
        </List>
    }
}