import React from 'react';
import PropTypes from 'prop-types';
import LocationClass from "../classes/Location.class";
import Chip from "@material-ui/core/es/Chip/Chip";
import HomeIcon from '@material-ui/icons/Home';
import PlaceIcon from '@material-ui/icons/Place';
import FlagIcon from '@material-ui/icons/Flag';


export default class Location extends React.Component {
    static propTypes = {
        location: PropTypes.instanceOf(LocationClass).isRequired,
        handleDelete: PropTypes.func,
        type: PropTypes.number
    };

    static defaultProps = {
        type: 0
    };

    constructor(props) {
        super(props);
        const icons = [<PlaceIcon/>, <HomeIcon/>, <FlagIcon/>];
        if (this.props.handleDelete)
            this.state = {
                chipProps: {
                    icon: icons[this.props.type],
                    label: this.props.location.label,
                    onDelete: this.handleDelete
                }
            };
        else
            this.state = {
                chipProps: {
                    icon: icons[this.props.type],
                    label: this.props.location.label
                }
            };
    }

    handleDelete = () => {
        this.props.handleDelete(this.props.location);
    };

    render() {
        return <Chip {...this.state.chipProps}/>
    }
}