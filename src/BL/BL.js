import strToLoc from "./services/_geocoder.BL";
import getDistancesMatrix from "./services/_distances.BL";
import getBestPath from "./services/_pathFinder.BL";
import RouteClass from "../classes/Route.class";
import LocationClass from '../classes/Location.class';
import stringToSuggestions from './services/_autoComplete.BL';
import {debounce} from "./utils.BL";

/**
 * Get the best route to travel all locations, from start to end (if exists) or any random location.
 * @param route - RouteClass object with start & locations
 * @returns RouteClass Object
 */
export const getBestRoute = (route) => {
    const locations = [route.start, ...route.locations, route.end].filter((l) => l);
    return getDistancesMatrix(locations.map((l) => (l.geoString))).then((matrix) => {
        const pathData = getBestPath(matrix, 0, route.end ? locations.length - 1 : undefined);
        const start = locations[pathData.path[0]];
        const end = locations[pathData.path[pathData.path.length - 1]];
        let pathLocations = [];
        for (let i = 1; i < pathData.path.length - 1; i++)
            pathLocations.push(locations[pathData.path[i]]);
        return new RouteClass({start, end, locations: pathLocations, distance: pathData.distance});
    });
};

export const stringToLocation = (location) => {
    return strToLoc(location).then(({longitude, latitude, address}) => {
        return new LocationClass({longitude, latitude, address});
    });
};

export const getSuggestions = (query, n = 3) => {
    return stringToSuggestions(query, n).then((addresses) => {
        const labels = addresses.map((a) => {
            return (LocationClass.getLabel({
                Street: a.street, City: a.city, HouseNumber: a.houseNumber
            }));
        });
        const noNulls = labels.filter(item => item);
        return Array.from(new Set(noNulls));
    });
};

export const debounceFunc = debounce;