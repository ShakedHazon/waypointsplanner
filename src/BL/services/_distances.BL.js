import {appCode, appId} from '../../passwords';
import {dictToUrlParams} from '../utils.BL';

const getMatrixData = (coordinates) => {
    const baseUrl = 'https://matrix.route.api.here.com/routing/7.2/calculatematrix.json?';
    let params = {
        app_id: appId,
        app_code: appCode,
        mode: 'fastest;car;traffic:disabled',
        summaryAttributes: 'distance'
    };
    for (let i = 0; i < coordinates.length; i++) {
        params[`start${i}`] = coordinates[i];
        params[`destination${i}`] = coordinates[i];
    }

    const url = `${baseUrl}${dictToUrlParams(params)}`;
    return fetch(url).then((response) => response.json());
};

/**
 * Get distances matrix between n locations
 * @param coordinates - n strings representing geo locations in template geo!Lat,Lon
 * @returns n*n matrix, where m[i][j] includes the distance between i and j
 */
const getDistancesMatrix = (coordinates) => {
    return getMatrixData(coordinates).then((data) => {
        let matrix = {};
        for (const key in data.response.matrixEntry) {
            if (!key) continue;
            const entry = data.response.matrixEntry[key];
            const start = entry.startIndex;
            if (!matrix[start])
                matrix[start] = {};
            const destination = entry.destinationIndex;
            const distance = entry.summary.distance;
            matrix[start][destination] = start === destination ? 0 : distance;
        }
        return matrix;
    });
};

export default getDistancesMatrix;