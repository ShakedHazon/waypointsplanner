/**
 * Get the best path to take which starts in 'start' and passes all other locations in matrix
 * @param matrix - n*n matrix in which m[i][j] is the distance between i and j
 * @param start - index of the start of the route, defaults to 0
 * @param end - index of last stop of the route. If not defined - any stop except start is selected
 * @returns {{path, distance}} - path: list of the indexes in the best route, distance: total distance
 */
const getBestPath = (matrix, start = 0, end = undefined) => {
    let stops = [...Array(Object.keys(matrix).length).keys()];
    stops.splice(start, 1);
    if (typeof end !== 'undefined')
        stops.splice(end - 1, 1);
    return getBestPathRecursive(matrix, [start], stops, end, 0);
};

const getBestPathRecursive = (matrix, path, left, end, distance) => {
    if (left.length === 0 && end) {
        distance += matrix[path[path.length - 1]][end];
        path = [...path, end];
    }
    if (left.length === 0)
        return {path: path, distance: distance};

    let minDistance = Number.MAX_SAFE_INTEGER;
    let solution = undefined;

    for (const key in left) {
        if (typeof left[key] === 'undefined') continue;
        const nextStep = left[key];
        let newLeft = [...left];
        newLeft.splice(left.indexOf(nextStep), 1);

        const currentSol = getBestPathRecursive(matrix,
            [...path, nextStep],
            newLeft,
            end,
            distance + matrix[path[path.length - 1]][nextStep]);

        if (currentSol.distance < minDistance) {
            solution = currentSol;
            minDistance = solution.distance;
        }

    }
    return solution;
};
export default getBestPath;