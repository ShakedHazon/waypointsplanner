import {appCode, appId} from '../../passwords';
import {dictToUrlParams, cleanQuery} from '../utils.BL';
import t from 'typy';

const stringToResponse = (address) => {
    const baseUrl = 'https://geocoder.api.here.com/6.2/geocode.json?';
    const params = {
        app_id: appId,
        app_code: appCode,
        searchtext: cleanQuery(address)
    };
    const url = `${baseUrl}${dictToUrlParams(params)}`;
    return fetch(url).then((response) => response.json());
};

/**
 * Transform string to LocationClass object using Where gecodoer
 * @param address - input string
 * @returns {longitude, latitude, label}
 */
const stringToLocation = (address) => {
    return stringToResponse(address).then((responseJson) => {
        const {Longitude: longitude, Latitude: latitude} = t(responseJson, 'Response.View[0].Result[0].Location.DisplayPosition').safeObject;
        const address = t(responseJson, 'Response.View[0].Result[0].Location.Address').safeObject;
        return {longitude, latitude, address};
    });
};

export default stringToLocation;