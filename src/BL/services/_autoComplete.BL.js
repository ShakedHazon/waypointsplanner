import {appCode, appId} from '../../passwords';
import {dictToUrlParams, cleanQuery} from '../utils.BL';

const stringToResponse = (query, n) => {
    const baseUrl = 'https://autocomplete.geocoder.api.here.com/6.2/suggest.json?';
    const params = {
        app_id: appId,
        app_code: appCode,
        query: cleanQuery(query),
        maxresults: n
    };
    const url = `${baseUrl}${dictToUrlParams(params)}`;
    return fetch(url).then((response) => response.json());
};

const stringToSuggestions = (query, n = 3) => {
    return stringToResponse(query, n).then((response) => {
        return response.suggestions.map((s) => (s.address));
    });
};

export default stringToSuggestions;