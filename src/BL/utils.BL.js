export const dictToUrlParams = (dict) => {
    let str = [];
    for (const p in dict)
        str.push(`${p}=${dict[p]}`);
    return str.join("&");
};

export const debounce = (fn, delay) => {
    // Each call to the returned anonymous function will share this common variable
    let timedOutFunc = null;
    return function () {
        let context = this, args = arguments;
        // Prevent other function in previous timeout to be executed
        clearTimeout(timedOutFunc);
        timedOutFunc = setTimeout(function () {
            fn.apply(context, args);
        }, delay);
    };
};

export const cleanQuery = (query) => (query.replace(' ', '+')); // ToDo something more general?);