import React from 'react';
import ReactDOM from 'react-dom';
import 'normalize.css/normalize.css';
import './styles/styles.scss';
import WaypointsApp from './components/WaypointsApp';
import './styles/styles.scss';

ReactDOM.render(<WaypointsApp/>, document.getElementById('app'));
