import getBestPath from '../BL/services/_pathFinder.BL';

const buildMatrix = (distances) => {
    let m = {};
    for (let i = 0; i < distances.length; i++) {
        m[i] = {};
        for (let j = 0; j < distances[i].length; j++)
            m[i][j] = distances[i][j];
    }
    return m;
};

it('should return basic path', () => {
    const m = buildMatrix([
        [0, 10, 20],
        [10, 0, 30],
        [20, 30, 0]]);
    const solution = getBestPath(m, 0);
    expect(solution).toBeTruthy();
    expect(solution.path).toEqual([0, 1, 2]);
    expect(solution.distance).toBe(40);
});

it('should solve dijsktra example', () => {
    // https://www.geeksforgeeks.org/travelling-salesman-problem-set-1/
    const m = buildMatrix([
        [0, 10, 15, 20],
        [10, 0, 35, 25],
        [15, 35, 0, 30],
        [20, 25, 30, 0]
    ]);
    const solution = getBestPath(m, 0);
    expect(solution).toBeTruthy();
    expect(solution.path).toEqual([0, 1, 3, 2]);
    expect(solution.distance).toBe(65);
});

it('should solve another dijsktra example', () => {
    // https://www.geeksforgeeks.org/travelling-salesman-problem-set-1/
    debugger;
    const m = buildMatrix([
        [0, 12, 10, 19, 8],
        [12, 0, 3, 7, 2],
        [10, 3, 0, 6, 20],
        [19, 7, 6, 0, 4],
        [8, 2, 20, 4, 0]
    ]);
    const solution = getBestPath(m, 1);
    expect(solution).toBeTruthy();
    expect(solution.path).toEqual([1, 2, 3, 4, 0]);
    expect(solution.distance).toBe(21);
});
